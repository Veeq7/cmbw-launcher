#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "git_utils.h"
#include "windows_utils.h"

// libgit2-1.5.0 built using:
// cmake -G "Visual Studio 17 2022" -A x64 -DBUILD_SHARED_LIBS=OFF -DBUILD_CLAR=OFF ..
// and then manually built on MinSizeRel by using the .sln file
// unfortunately `cmake --build .` builds in Debug, even when `-DCMAKE_BUILD_TYPE=MinSizeRel` is specified

#define ArrayCount(x) (sizeof((x)) / sizeof(*(x)))

struct RepoInfo {
	const char* name;
	const char* repo_url;
	const char* exe_name;
};

RepoInfo repos[] = {
	{ "CMBW Release",	 "https://gitlab.com/the-no-frauds-club/cosmonarchy-bw-release/",    "Cosmonarchy BW.exe" },
	{ "CMBW Prerelease", "https://gitlab.com/the-no-frauds-club/cosmonarchy-bw-prerelease/", "Cosmonarchy BW.exe" },
};

static const char* starcraft_reg_path = "SOFTWARE\\Wow6432Node\\Blizzard Entertainment\\Starcraft";
static const char* defender_reg_path = "SOFTWARE\\Microsoft\\Windows Defender\\Exclusions\\Paths";

const char* get_starcraft_exe_path() {
	const char starcraft_exe_path[256 + 1] = {};
    open_path("Choose Starcraft 1.16.1 executable file...", "Starcraft 1.16.1 executable (StarCraft.exe)\0StarCraft.exe\0\0", (char*)starcraft_exe_path, 256);
	return starcraft_exe_path;
}

const char* get_starcraft_dir_path(const char* starcraft_exe_path) {
	static char starcraft_dir_path[256 + 1] = {};
	strcpy_s(starcraft_dir_path, starcraft_exe_path);

	char* last_slash = strrchr(starcraft_dir_path, '\\');
	if (last_slash) {
		*last_slash = 0;
	}
	return starcraft_dir_path;
}

void menu() {
	bool had_errors = false;

	int user_choice = 0;
	int all = ArrayCount(repos) + 1;
	int regfix = all + 1;
	while (user_choice <= 0 || user_choice > regfix) {
		for (int i = 0; i < ArrayCount(repos); ++i) {
			printf("%u - %s at %s\n", i + 1, repos[i].name, repos[i].repo_url);
		}
		if (ArrayCount(repos) == 2) {
			printf("%u - Both\n", all);
		} else if (ArrayCount(repos) > 2) {
			printf("%u - All\n",  all);
		}
		printf("%u - Regfix\n", regfix);
		scanf_s("%u", &user_choice);
	}

	if (user_choice == regfix) {
		const char* starcraft_exe_path = get_starcraft_exe_path();
		const char* starcraft_dir_path = get_starcraft_dir_path(starcraft_exe_path);

		registry_set_string(HKEY_LOCAL_MACHINE, starcraft_reg_path, "Program",     starcraft_exe_path);
		registry_set_string(HKEY_LOCAL_MACHINE, starcraft_reg_path, "InstallPath", starcraft_dir_path);
		registry_set_string(HKEY_LOCAL_MACHINE, starcraft_reg_path, "StarEdit",    starcraft_dir_path);

		registry_set_dword(HKEY_LOCAL_MACHINE, defender_reg_path, starcraft_dir_path, 0);
		registry_set_dword(HKEY_LOCAL_MACHINE, defender_reg_path, starcraft_exe_path, 0);
	} else {
		for (int i = 0; i < ArrayCount(repos); ++i) {
			if (user_choice == i + 1 || user_choice == all) {
				RepoInfo* repo_info = &repos[i];
				if (create_or_update_repo(repo_info->name, repo_info->repo_url)) {
					char from[256] = {};
					sprintf_s(from, "%s.lnk", repo_info->name);

					char current_directory[256] = {};
					GetCurrentDirectoryA(256, current_directory);

					char to[256] = {};
					sprintf_s(to, "%s\\%s\\%s", current_directory, repo_info->name, repo_info->exe_name);

					char working_directory[256] = {};
					sprintf_s(working_directory, "%s\\%s\\", current_directory, repo_info->name);

					char description[256] = {};
					sprintf_s(description, "Run %s", repo_info->name);

					create_link(from, to, working_directory, description);

					registry_set_dword(HKEY_LOCAL_MACHINE, defender_reg_path, working_directory, 0);
					registry_set_dword(HKEY_LOCAL_MACHINE, defender_reg_path, to,                0);
				} else {
					had_errors = true;
				}
			}
		}
	}

	if (had_errors) {
		printf("Something went wrong...\n");
		char buffer[32];
		scanf_s("%s", buffer, 32);
	}
}

int main() {
	win_init();
	git_init();

    menu();

	git_shutdown();
	win_shutdown();
	return 0;
}