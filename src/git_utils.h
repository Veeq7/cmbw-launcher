#pragma once

void git_init();
void git_shutdown();
bool create_or_update_repo(const char* directory, const char* repo_url);