#include "git_utils.h"
#include "windows_utils.h"

#include <memory>
#include <stdio.h>
#include <assert.h>
#include "include/git2.h"

static int last_progress;

static bool git_assert(int error_code) {
	if (error_code) {
		printf("%s\n", git_error_last()->message);
	}
	return !error_code;
}

static char* format_size(int size) {
	static char buffer[32];
	if (size < 1024) {
		sprintf_s(buffer, "%.1fB", (float)size);
	} else if (size < 1024 * 1024) {
		sprintf_s(buffer, "%.1fKB", (float)size / 1024);
	} else if (size < 1024 * 1024 * 1024) {
		sprintf_s(buffer, "%.1fMB", (float)size / 1024 / 1024);
	} else {
		sprintf_s(buffer, "%.1fGB", (float)size / 1024 / 1024 / 1024);
	}
	return buffer;
}

static int transfer_progress_callback(const git_indexer_progress* stats, void* payload) {
	if (stats->total_deltas) {
		int progress = 100 * (float)stats->indexed_deltas / stats->total_deltas;
		if (progress != last_progress) {
			last_progress = progress;
			printf("Resolving deltas... %u%%\n", progress);
		}
	} else {
		int progress = 100 * ((float)stats->received_objects + stats->indexed_objects) / (stats->total_objects * 2);
		if (progress != last_progress) {
			last_progress = progress;
			printf("Downloading... %u%% | %s\n", progress, format_size(stats->received_bytes));
		}
	}
	return 0;
}

static int fetchhead_callback(const char* ref_name, const char* remote_url, const git_oid* oid, unsigned int is_merge, void *payload) {
	git_oid_cpy((git_oid*)payload, oid);
	return 0;
}

void git_init() {
	git_libgit2_init();
}

void git_shutdown() {
	git_libgit2_shutdown();
}

template <typename T, typename Deleter>
static std::unique_ptr<T, Deleter> adopt(T* ptr, Deleter deleter) {
	return {ptr, deleter};
}

static auto open_repo(const char* directory) {
	git_repository* repo = 0;
	git_assert(git_repository_open(&repo, directory));
	return adopt(repo, git_repository_free);
}

static auto clone_repo(const char* directory, const char* repo_url, const git_clone_options& clone_options) {
	git_repository* repo = 0;
	git_assert(git_clone(&repo, repo_url, directory, &clone_options));
	return adopt(repo, git_repository_free);
}

static auto lookup_remote(git_repository* repo, const char* name) {
	git_remote* remote = 0;
	git_assert(git_remote_lookup(&remote, repo, "origin"));
	return adopt(remote, git_remote_free);
}

static auto lookup_object(git_repository* repo, const git_oid& oid, git_object_t type) {
	git_object* object = 0;
	git_assert(git_object_lookup(&object, repo, &oid, type));
	return adopt(object, git_object_free);
}

bool try_update_repo(const char* directory, const char* repo_url, const git_fetch_options& fetch_options) {
	printf("Looking up the repo...\n");
	auto repo = open_repo(directory);
	if (!repo) {
		return false;
	}

	printf("Looking up the remote...\n");
	auto remote = lookup_remote(repo.get(), "origin");
	if (!remote) {
		return false;
	}

	printf("Confirming correct remote url...\n");
	if (strcmp(git_remote_url(remote.get()), repo_url) != 0) {
		return false;
	}

	printf("Fetching remote data...\n");
	if (!git_assert(git_remote_fetch(remote.get(), 0, &fetch_options, 0))) {
		return false;
	}

	printf("Fetching newest commit...\n");
	git_oid commit_oid = {};
	if (!git_assert(git_repository_fetchhead_foreach(repo.get(), fetchhead_callback, &commit_oid))) {
		return false;
	}

	printf("Looking up the newest commit object...\n");
	auto commit_object = lookup_object(repo.get(), commit_oid, GIT_OBJECT_COMMIT);
	if (!commit_object) {
		return false;
	}

	printf("Resetting to the newest commit...\n");
	if (!git_assert(git_reset(repo.get(), commit_object.get(), GIT_RESET_HARD, 0))) {
		return false;
	}

	return true;
}

bool create_or_update_repo(const char* directory, const char* repo_url) {
	git_fetch_options fetch_options = GIT_FETCH_OPTIONS_INIT;
	fetch_options.callbacks.transfer_progress = transfer_progress_callback;

	printf("> %s at %s\n", directory, repo_url);
	if (try_update_repo(directory, repo_url, fetch_options)) {
		return true;
	}
	
	printf("Cloning the repo...\n");
	delete_directory(directory);
	git_clone_options clone_options = GIT_CLONE_OPTIONS_INIT;
	clone_options.fetch_opts = fetch_options;
	auto repo = clone_repo(directory, repo_url, clone_options);
	if (!repo) {
		return false;
	}

	return true;
}
