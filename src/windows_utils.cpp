#include "windows_utils.h"

#include <stdio.h>
#include <assert.h>

#include <Windows.h>
#include <winhttp.h>
#include <shobjidl.h>
#include <shlguid.h>

void win_init() {
	CoInitialize(0);
}

void win_shutdown() {
    CoUninitialize();
}

void create_link(const char* from, const char* to, const char* working_directory, const char* description) {
	assert(to && from);
	printf("Linking from %s to %s...\n", from, to);

	IShellLinkA* shell_link = 0;
	if (SUCCEEDED(CoCreateInstance(CLSID_ShellLink, 0, CLSCTX_INPROC_SERVER, IID_IShellLink, (void**)&shell_link))) {
		shell_link->SetPath(to);
		if (working_directory) shell_link->SetWorkingDirectory(working_directory);
		if (description)       shell_link->SetDescription(description);
		IPersistFile* persist_file = 0;
		if (SUCCEEDED(shell_link->QueryInterface(IID_IPersistFile, (void**)&persist_file))) {
			WCHAR unicode_path[MAX_PATH] = {};
			MultiByteToWideChar(CP_ACP, 0, from, -1, unicode_path, MAX_PATH);
			persist_file->Save(unicode_path, true);
			persist_file->Release();
		}
		shell_link->Release();
	}
}

void delete_directory(const char* path) {
    SHFILEOPSTRUCTA file_operation = {};
    file_operation.wFunc = FO_DELETE;
    file_operation.pFrom = path;
    file_operation.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT;
    SHFileOperationA(&file_operation);
}

void registry_set_string(HKEY key, const char* subkey, const char* name, const char* value) {
	RegSetKeyValueA(key, subkey, name, REG_SZ, value, strlen(value) + 1);
}

void registry_set_dword(HKEY key, const char* subkey, const char* name, unsigned int value) {
	RegSetKeyValueA(key, subkey, name, REG_DWORD, &value, sizeof(value));
}

void open_path(const char* title, const char* filter, char* buffer, size_t buffer_len) {
	OPENFILENAMEA openfile = {};
	openfile.lStructSize = sizeof(openfile);
	openfile.lpstrFilter = filter;
	openfile.lpstrFile = (LPSTR)buffer;
	openfile.nMaxFile = buffer_len;
	openfile.lpstrTitle = title;
	openfile.Flags = OFN_FILEMUSTEXIST;
	GetOpenFileNameA(&openfile);
}
