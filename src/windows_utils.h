#pragma once
#include <Windows.h>

void win_init();
void win_shutdown();

void create_link(const char* from, const char* to, const char* working_directory, const char* description);
void delete_directory(const char* path);
void registry_set_string(HKEY key, const char* subkey, const char* name, const char* value);
void registry_set_dword(HKEY key, const char* subkey, const char* name, unsigned int value);
void open_path(const char* title, const char* filter, char* buffer, size_t buffer_len);
